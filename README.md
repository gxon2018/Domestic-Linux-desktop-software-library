# 国产Linux桌面操作系统软件库

#### 介绍
本仓库是为国产Linux操作系统搜罗的软件仓库，主要是为了方便国产Linux用户方便找到可代替日常办公、生活、娱乐、及创作方面的软件，本仓库搜罗的软件来自互联网，不确保所有软件的可用性和安全性，请大家自行甄别。

软件持续更新中，所有软件排名不分先后！

#### 软件分类
软件分类说明


#### 输入法

1.  搜狗输入法-[官网](https://shurufa.sogou.com/linux)


- 输入支持：全拼、双拼（智能ABC、国标双拼、微软双拼、拼音加加、搜狗双拼、小鹤双拼、紫光双拼、自然码）、五笔（86、98、新世纪）
- 支持系统：Ubuntu(2004/1910/1804/1604)、Ukylin、Deepin、麒麟、UOS
- 支持架构：x86_64、arm64、mips64el、loongarch64
- 目前版本：V4.2.1（2023-04-03更新）


2.  百度输入法-[官网](https://srf.baidu.com/site/guanwang_linux/index.html)


- 输入支持：全拼、双拼、五笔
- 支持系统：Ubuntu(2004/1910/1804/1604)、Ukylin、Deepin、麒麟、UOS
- 支持架构：x86_64、arm64
- 目前版本：2023



3.  讯飞输入法-[官网](https://srf.xunfei.cn/index.html#/)

- 输入支持：全拼、双拼、语音
- 支持系统：Ubuntu(2004/1910/1804/1604)、Ukylin、Deepin、麒麟、UOS
- 支持架构：x86_64、arm64、mips64el、loongarch64
- 目前版本：3.0.51


#### 社交

1.  微信Linux - [官网]()


2.  QQ for linux - [官网](https://im.qq.com/linuxqq/index.shtml)


3.  Skype - [官网](https://go.skype.com/skypeforlinux-64.deb)


#### 办公文档

1.  WPS Office - [官网](https://www.wps.cn/product/wpslinux)


2.  金山文档


3.  腾讯文档


4.  LibreOffice - [官网](https://zh-cn.libreoffice.org/)

LibreOffice 是一款功能强大的办公软件，默认使用开放文档格式 (OpenDocument Format , ODF), 并支持 *.docx, *.xlsx, *.pptx 等其他格式。

- 它包含了 Writer, Calc, Impress, Draw, Base 以及 Math 等组件，可用于处理文本文档、电子表格、演示文稿、绘图以及公式编辑。
- 它可以运行于 Windows, GNU/Linux 以及 macOS 等操作系统上，并具有一致的用户体验。
- 特点：自由免费的全能办公套件。对个人和企业均免费，不用支付授权费用！



5.  OnlyOffice - [官网](https://www.onlyoffice.com/zh/)

适用于团队合作的在线办公套件，支持桌面端和移动端等多平台。
- 特点：免费


6.  Apache OpenOffice - [官网](https://www.openoffice.org/download/index.html)

Apache OpenOffice 与其他主要办公套件兼容，可免费下载、使用和分发。

- Writer 一个文字处理器，你可以用它来做任何事情，从写一封简短的信到制作一整本书。
- Calc 一个功能强大的电子表格，其中包含计算、分析和以数字报告或炙手可热的图形形式呈现数据所需的所有工具。
- 以最快、最强大的方式创建有效的多媒体演示。
- Draw 可让您制作从简单图表到动态 3D 插图的所有内容。
- Base 允许您无缝操作数据库。在 Apache OpenOffice 中创建和修改表、表单、查询和报告。
- Math 允许您使用图形用户界面或直接在公式编辑器中键入公式来创建数学公式。




#### 视频会议

1.  腾讯会议 - [官网](https://meeting.tencent.com/download?mfrom=OfficialIndex_TopBanner1_Download)


2.  钉钉 - [官网](https://page.dingtalk.com/wow/z/dingtalk/simple/ddhomedownload)


3.  瞩目 - [官网](https://zhumu.com/download)


#### 笔记

1.  Typora（收费版） - [官方中文站](https://typoraio.cn/)

一款 Markdown 编辑器和阅读器

- 89元 / 3台设备 / 免费升级
- 风格极简 / 多种主题 / 支持 macOS，Windows 及 Linux
- 实时预览 / 图片与文字 / 代码块 / 数学公式 / 图表
- 目录大纲 / 文件管理 / 导入与导出 ……



2.  Typora（免费版）

Typora正式版 1.0 开始收费，0.11.18 为最后免费版本。


3.  Obsidian-黑曜石笔记 - [官网](https://obsidian.md/download)

黑曜石是一款私密且灵活的写作应用程序，可适应您的思维方式。


4.  Joplin - [官网](https://joplinapp.org/)

Joplin 是一个开源的笔记应用程序。捕捉您的想法，并从任何设备安全地访问它们。


5.  有道云笔记


6.  印象笔记


7.  我来 wolai


8.  Notesnook - [官网](https://notesnook.com/downloads/)


开源的端到端自由笔记软件


#### 文本编辑

1.  NotePad-- 开源免费的轻量级文本编辑器-[官网](https://gitee.com/cxasm/notepad--/releases/)

Notepad-- 是使用C++编写的轻量级文本编辑器, 简称ndd, 可以支持Window/Mac/Linux操作系统平台。
作者的目标：完成文本编辑类软件的国产可替代，重点在国产Uos/Linux系统、Mac 系统上发展。
对比其它竞品Notepad类软件而言，我们的优势是可以跨平台，支持linux mac操作系统。


2.  VIM

学过Linux的应该都非常熟悉它


3.  KWrite


#### 翻译

1.  腾讯交互翻译TranSmart - [官网](https://transmart.qq.com/zh-CN/download)


2.  Pot翻译 - [官网]（https://pot-app.com/）


3.  


#### 截屏录屏

1.  Flameshot - [官网](https://flameshot.org/)

Powerful, yet simple to use open-source screenshot software.

2.  OBS-Studio - [官网](https://obsproject.com/)

用于视频录制和直播的免费开源软件。


#### 图像图形

1.  gimp - [官网](https://www.gimp.org/downloads/)

GIMP 是一个跨平台的图像编辑器，可用于 GNU/Linux， macOS、Windows 和更多操作系统。它是免费的 软件，您可以更改其源代码并分发您的更改。
无论您是平面设计师、摄影师、插画家，还是 科学家，GIMP为您提供复杂的工具来获得工作 做。您可以使用 GIMP 进一步提高您的生产力，这要归功于 许多自定义选项和第三方插件。

2.  Krita - [官网](https://krita.org/zh/)

Krita 是一款自由开源的免费绘画软件，无需注册、无广告、试用期或者商用限制，让每一位画师都可以有尊严地表达创意。

- 适合：概念草图、插画、漫画、动画、接景和 3D 贴图
- 支持：数位板、压感、防抖、图层、滤镜、色彩管理等
- 中文：软件、文档和网站内建官方中文版
- 评价：最像PS的开源位图编辑软件


3.  Inkscape - [官网](https://inkscape.org/)

nkscape 是一个流行的开源应用程序，用于创建和编辑矢量图。它是一个功能丰富的矢量图形编辑器，这使它可以与其他类似的专有应用程序（如 Adobe Illustrator 和 Corel Draw）相竞争。正因为如此，许多专业插画师使用它来创建基于矢量的艺术作品。


4.  darktable - [官网](https://www.darktable.org/)

darktable 是摄影师或那些想提高照片质量的人的完美选择。darktable 更侧重于图像编辑，特别是对 RAW 图像的非破坏性后期制作。因此，它提供专业的色彩管理，支持自动检测显示配置文件。此外，你还可以用 darktable 过滤和排序多张图片。所以你可以通过标签、评级、颜色标签等来搜索你的收藏。它可以导入各种图像格式，如 JPEG、CR2、NEF、HDR、PFM、RAF 等。


#### 影音编辑

1.  Kdenlive - [官网](https://kdenlive.org/)

Kdenlive 是 KDE 上的一个自由且开源的视频编辑软件，支持双视频监控、多轨时间线、剪辑列表、自定义布局、基本效果，以及基本过渡效果。
它支持多种文件格式和多种摄像机、相机，包括低分辨率摄像机（Raw 和 AVI DV 编辑）、mpeg2、mpeg4 和 h264 AVCHD（小型相机和便携式摄像机）、高分辨率摄像机文件（包括 HDV 和 AVCHD 摄像机）、专业摄像机（包括 XDCAM-HD™ 流、IMX™ (D10) 流、DVCAM (D10)、DVCAM、DVCPRO™、DVCPRO50™ 流以及 DNxHD™ 流）。

2.  OpenShot - [官网](https://www.openshot.org/)

OpenShot 是 Linux 上的另一个多用途视频编辑器。OpenShot 可以帮助你创建具有过渡和效果的视频。你还可以调整声音大小。当然，它支持大多数格式和编解码器。
你还可以将视频导出至 DVD，上传至 YouTube、Vimeo、Xbox 360 以及许多常见的视频格式。OpenShot 比 Kdenlive 要简单一些。因此，如果你需要界面简单的视频编辑器，OpenShot 是一个不错的选择。

3.  Shotcut - [官网](https://www.shotcut.org/)

Shotcut 是 Linux 上的另一个编辑器，可以和 Kdenlive 与 OpenShot 归为同一联盟。虽然它确实与上面讨论的其他两个软件有类似的功能，但 Shotcut 更先进的地方是支持 4K 视频。
支持许多音频、视频格式，过渡和效果是 Shotcut 的众多功能中的一部分。它也支持外部监视器。

4.  Flowblade - [github](http://jliljebl.github.io/flowblade/)

Flowblade 是 Linux 上的一个多轨非线性视频编辑器。与上面讨论的一样，这也是一个自由开源的软件。它具有时尚和现代化的用户界面。

5.  Lightworks - [官网](https://lwks.com/)

通过强大而多功能的视频编辑释放您的创造力。从 YouTube 到好莱坞，Lightworks 为任何技能水平的视频编辑提供支持。
有免费和收费版本，个人版本免费。

6.  Blender - [官网](https://www.blender.org/)

Blender 是一个专业的，工业级的开源跨平台视频编辑器。它在制作 3D 作品的工具当中较为流行。Blender 已被用于制作多部好莱坞电影，包括蜘蛛侠系列。

7.  Cinelerra - [官网](http://cinelerra.org/)



8.  Cinelerra - [sourceforge](https://sourceforge.net/projects/heroines/)

只有源码包

9.  DaVinci Resolve - [官网](http://www.blackmagicdesign.com/products/davinciresolve)

如果你想要好莱坞级别的视频编辑器，那就用好莱坞正在使用的专业工具。来自 Blackmagic 公司的 DaVinci Resolve 就是专业人士用于编辑电影和电视节目的专业工具。
DaVinci Resolve 不是常规的视频编辑器。它是一个成熟的编辑工具，在这一个应用程序中提供编辑，色彩校正和专业音频后期制作功能。
有免费和收费版本，个人版本免费。

#### 工业设计

1.  中望CAD - [官网](https://www.zwcad.com/)

一款国产cad软件


2.  浩辰CAD - [官网](https://www.gstarcad.com/)

一款国产cad软件


3.  开源 CAD/CAE工具 FreeCAD - [官网](https://www.freecad.org/downloads.php?lang=zh_CN)

FreeCAD是一个基于OpenCASCADE的开源CAD/CAE工具。 OpenCASCADE是一套开源的CAD/CAM/CAE几何模型核心，来自法国Matra Datavision公司，是著名的CAD软件EUCLID的开发平台。

4.  CAD 应用程序 QCAD

QCad 是一个专业的 CAD 系统。QCad 使用户能够创建技术图纸，如计划、建筑、室内装饰、机械部件或架构和图表。它采用 DXF 文件作为其标准的文件格式。虽然其它 CAD 软件包往往是使用复杂，但 QCad 试图有所不同。

5.  计算机辅助设计系统 BRL-CAD

BRL-CAD 是一个构造实体几何(CSG) 实体模型计算机辅助设计(CAD) 系统。BRL-CAD 包括一个交互式的几何编辑器，光学跟踪支持图形着色和几何分析，计算机网络分布式帧缓存支持，图像处理和信号处理工具, 可以进行几何编辑、几何分析，支持分布式网络，可以进行图像处理和信号处理。

6.  3D建模软件 OpenSCAD

为有创造力的web开发者而设计，OpenSCAD是一个创建立体3D CAD物体的得力软件。
不像大多数穿件3D模型的的自由软件，它不专注于3D模型的艺术方面而是CAD方面。

7.  三维的 CAD/CAM 程序 gCAD3D

gCAD3D 是个轻量级的三维 CAD/CAM 程序，集成3D OpenGL浏览器、几何和三维数控命令功能，支持各种文件类型（Step，Iges，DXF，VRML，SVG）。

8.  2D 的 CAD 制图软件 LibreCAD

LibreCAD 是一款开源免费的 2D CAD 制图软件，原名为 CADuntu 。它是基于社区版本 QCad 构建，并利用 Qt4 进行了重构，原生支持 Mac OSX, Windows 和 Linux 。它提供了基于 GPL 协议的读取/修改/创建 CAD 文件 (.dxf ) 方案。

9.  2D CAD 软件 DraftSight

DraftSight 是免费 2D CAD软件，可用于Windows®和Mac®，向专业CAD用户以及学生和教师提供创建、编辑和观看DWG文件更佳途径，是2D CAD的最佳选择！

10.  用于创建虚拟乐高模型的开源 CAD 程序 LeoCAD

LeoCAD是一个用于创建虚拟乐高模型的CAD程序。它是可用的或者在GNU公共许可下自由运行在Windows上，Linux上还有Mac OSX操作系统。

11.  计算机辅助设计软件 BlocksCAD

BlocksCAD 是来自 H3XL 公司的一个开源的 CAD 计算机辅助设计软件。BlocksCAD 是基于 Node.js 开发，使用浏览器进行访问。

12.  Wings 3D

Wings 3D是一款免费的开源软件。它提供了各种各样的建模工具，可以帮助您完成所有项目。它是创建CAD模型并处理纹理和材料的好软件。但是，如果您需要用于动画和渲染的CAD程序，则它不是完美的工具。

13.  


#### 视频播放

1.  腾讯视频


2.  优酷


3.  哔哩哔哩


#### 音乐电台

1.  QQ音乐


2.  酷狗音乐


3.  网易云音乐


#### 网络下载

1.  迅雷 - [官网](http://www.xunlei.com/)

迅雷官网未提供Linux版本下载，信创应用商店下载


2.  qBittorrent - [github](https://github.com/qbittorrent/qBittorrent)


3.  ktorrent - [官网](https://apps.kde.org/ktorrent/)


4.  Motrix - [官网](https://motrix.app/zh-CN/download)

一款全能的下载工具
支持下载 HTTP、FTP、BT、磁力链接等资源


5. XDM - [github](https://github.com/subhra74/xdm/releases)

Xtreme Download Manager（XDM）是一款功能强大的工具，可以将下载速度提高500%，保存来自流行视频流媒体网站的视频，恢复损坏/死亡的下载，安排和转换下载。
XDM与Google Chrome、Mozilla Firefox Quantum、Opera、Vivaldi和其他基于Chrome和Firefox的浏览器无缝集成，以接管下载和保存网络流媒体视频。XDM有一个内置的视频转换器，它可以让您将下载的视频转换为不同的格式，以便您可以在移动的或电视上观看它们（支持100+设备）


#### 网盘云盘

1.  百度云


2.  阿里云盘


3.  腾讯微云



#### 编程开发

1.  Visual Studio Code


2.  InetlliJ Pycharm


3.  InetlliJ IDEA


#### 虚拟化

1.  VMware Workstation - [官网](https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html)



2.  Oracle VM VirtualBox - [官网](https://www.virtualbox.org/)


3.  KVM - [官网](https://www.linux-kvm.org/page/Main_Page)


#### 数据库

1.  Navicat - [官网](https://navicat.com.cn/download/navicat-premium)


2.  DBeaver - [官网](https://dbeaver.io/)


3.  termine - [github](https://github.com/team-ide/teamide)


#### 桌面远程控制

1.  向日葵


2.  Todesk


3.  RustDesk


#### 服务器远程工具

1.  Windterm-[github](https://github.com/kingToolbox/WindTerm/releases)


2.  FinalShell-[官网](https://www.hostbuf.com/)


3.  Remmina-[官网](https://remmina.org/)


4.  SecureCRT-[官网](https://www.vandyke.com/download/)


5.  TeamIDE - [github](https://github.com/team-ide/teamide)


6.  XTerminal - [官网](https://www.xterminal.cn/)


7.  ishell - [官网](https://ishell.cc/)


#### 网络工具

1.  Nmap - [官网](https://nmap.org/)

Nmap（“网络映射器”）是一个免费的开源实用程序，用于 网络发现和安全审计。许多系统和网络 管理员还发现它对网络等任务很有用 清点、管理服务升级计划和监控主机或 服务正常运行时间。Nmap 以新颖的方式使用原始 IP 数据包来确定 网络上有哪些主机可用，哪些服务（应用程序 名称和版本）这些主机提供什么操作系统 （和操作系统版本），它们正在运行，什么类型的数据包 过滤器/防火墙正在使用中，以及许多其他特征。它 旨在快速扫描大型网络，但可以很好地 单个主机。Nmap 可在所有主流计算机操作系统上运行，并且 官方二进制包可用于 Linux、Windows 和 Mac OS X. 除了经典的命令行 Nmap 可执行文件外，Nmap 套件包括高级 GUI 和结果查看器 （Zenmap），一个灵活的数据 传输、重定向和调试工具 （Ncat），一个实用程序 比较扫描结果 （Ndiff） 以及数据包生成和响应分析工具 （Nping）。

```
# 安装
sudo apt install nmap
```


2.  Wireshark - [官网](https://www.wireshark.org/)

开源的数据包分析工具


3.  Tcpping - [官网](http://www.vdberg.org/~richard/tcpping)

tcp端口探测工具

```
# 安装
curl -O http://www.vdberg.org/~richard/tcpping
sudo mv tcpping /usr/bin/
```



4.  Tcpdump

Linux系统命令行抓包工具

```
# 安装
sudo apt install tcpdump
```


5.  gping - [github](https://github.com/orf/gping)

Linux命令行图形化ping工具,可同时ping多个IP

```
echo "deb [signed-by=/usr/share/keyrings/azlux-archive-keyring.gpg] http://packages.azlux.fr/debian/ stable main" | sudo tee /etc/apt/sources.list.d/azlux.list
sudo wget -O /usr/share/keyrings/azlux-archive-keyring.gpg  https://azlux.fr/repo.gpg
sudo apt update
sudo apt install gping
```


6.  speedtest - [命令行测试工具-官网](https://www.speedtest.net/apps/cli)

```
# 安装
sudo apt install speedtest-cli
# 测速
speedtest-cli

```


7.  iperf3 - [命令行的服务器与客户机带宽测试工具](https://iperf.fr/iperf-download.php)

```
# 安装
sudo apt install iperf3

```

8.  nload - [命令行流量监控工具]()

```
# 安装
sudo apt install nload
# 监控
nload -u K
```


#### 远程接入

1.  深信服EasyConnect - [官网](http://download.sangfor.com.cn/download/product/sslvpn/pkg/linux_01/EasyConnect_x64.deb)

```bash
# 下载
wget http://download.sangfor.com.cn/download/product/sslvpn/pkg/linux_01/EasyConnect_x64.deb
```


2.  奇安信sslVPN


3.  深信服aTurst


#### 打印驱动

1.  瑞印万能打印驱动（个人版）


2.  


3.  


####  应用商店

1.  星火应用商店 - [官网](https://spark-app.store/)

国产的应用商店，可下载并安装微信或QQ等国产软件，非常省心。

- 官网：https://snapcraft
- 发行版：https://snapcraft.io/docs/installing-snapd
- debian配置：https://snapcraft.io/docs/installing-snap-on-debian
- 应用查找：https://snapcraft.io/store
- 安装应用命令行：sudo snap install xxx



2.  Flatpak应用商店 - [官网](https://flathub.org/zh-Hans/apps/search)

获取适用于任何 Linux 发行版的所有常用应用程序的最新版本。

- 官网：https://flathub.org/
- 发行版：https://flathub.org/zh-Hans/setup
- Deepin配置：https://flathub.org/zh-Hans/setup/Deepin
- 应用查找：https://flathub.org/zh-Hans/apps/search
- 安装应用命令行：flatpak install flathub xxx
- 镜像加速：https://mirrors.sjtug.sjtu.edu.cn/docs/flathub



3.  snap应用商店 - [官网](https://snapcraft.io/store)


- 一个Linux下的软件商店，拥有丰富的软件生态。
- 官网：https://spark-app.store/
- 下载：https://spark-app.store/download
- 应用查找：https://spark-app.store/store
- 安装应用图形界面：略


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
7.  KDE开源社区[应用商店](https://apps.kde.org/zh-cn/)
